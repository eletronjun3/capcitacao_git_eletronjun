# Projeto exemplo capacitação

[Treinamento básico - utilizando os comandos do git](https://vinicius-mendes.gitbook.io/capacitacao-de-git/)

## Equipe
|Cargo    | Nome                              | GitLab                                                     |
| ----------  | ---------------------------------- | ---------------------------------------------------------- |
| Diretor de Relacionamentos  | Vinícius Mendes         | [@yabamiah1](https://gitlab.com/yabamiah1)                  |
| Gerente de Projetos | Gustavo Henrique     | [@GHRVO](https://gitlab.com/GHRVO)         |
| Gerente de Projetos | Ludmylla Martins     | [@ludmylla](https://gitlab.com/ludmylla)         |

## Objetivos

Realizar uma simulação praticando a utilização do Gitlab em um projeto da EletronJun. 

## Diagrama 


## Cronograma

