#ifndef Gus_temperature_H
#define Gus_temperature_H

#include <Arduino.h>

class Gus_temperature {
  public:
    Gus_temperature(int pin, int samplingInterval);
    float readTemperature();
    void begin();
  private:
    int _pin;
    int _samplingInterval;
    hw_timer_t *_timer;
    volatile bool _timerInterruptFlag;
    void IRAM_ATTR onTimerInterrupt();
};

#endif
