#ifndef LUDBAT_H
#define LUDBAT_H

   float tensaobat(int pino);

   float porcentagembat (int pino);
   
   float porcentagembat_semmap (int pino, float tensao_max);

#endif