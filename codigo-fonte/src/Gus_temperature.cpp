#include "Gus_temperature.h"

Gus_temperature::Gus_temperature(int pin, int samplingInterval) {
  _pin = pin;
  _samplingInterval = samplingInterval;
}

void Gus_temperature::begin() {
  Serial.begin(9600);
  analogReadResolution(12); // Configura a resolução de 12 bits para a leitura analógica
  _timer = timerBegin(0, 80, true);
  timerAttachInterrupt(_timer, &Gus_temperature::onTimerInterrupt, this);
  timerAlarmWrite(_timer, _samplingInterval, true); // Intervalo de amostragem definido pela variável _samplingInterval
  timerAlarmEnable(_timer);
}

float Gus_temperature::readTemperature() {
  float temperature = analogRead(_pin) * 3.3 / 4095 * 100; // Leitura da temperatura em graus Celsius
  return temperature;
}

void IRAM_ATTR Gus_temperature::onTimerInterrupt() {
  _timerInterruptFlag = true;
}
